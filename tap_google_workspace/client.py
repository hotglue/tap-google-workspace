"""REST client handling, including google-workspaceStream base class."""

import logging
from typing import Any, Dict, Optional

import requests
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

from tap_google_workspace.auth import OAuth2Authenticator

logging.getLogger("backoff").setLevel(logging.CRITICAL)


class googleWorkspaceStream(RESTStream):
    """google-workspace stream class."""

    url_base = "https://admin.googleapis.com/admin/"

    next_page_token_jsonpath = "$.nextPageToken"
    _page_size = 100

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        return OAuth2Authenticator(self, "https://oauth2.googleapis.com/token")

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        all_matches = extract_jsonpath(self.next_page_token_jsonpath, response.json())
        next_page_token = next(iter(all_matches), None)
        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["domain"] = self.config.get("domain")
        params["maxResults"] = self._page_size
        if next_page_token:
            params["pageToken"] = next_page_token
        return params
