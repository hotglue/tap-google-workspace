"""Stream type classes for tap-google-workspace."""

from typing import Optional
from singer_sdk import typing as th

from tap_google_workspace.client import googleWorkspaceStream

NESTED_PARAMETER = th.ObjectType(
        th.Property("name", th.StringType),
        th.Property("value", th.StringType),
        th.Property("multiValue", 
            th.ArrayType(
                th.StringType
            )
        ),
        th.Property("intValue", th.StringType),
        th.Property("multiIntValue", 
            th.ArrayType(
                th.StringType
            )
        ),
        th.Property("boolValue", th.BooleanType),
        th.Property("multiBoolValue", 
            th.ArrayType(
                th.BooleanType
            )
        ),
    )

ACTIVITIES_SCHEMA =  th.PropertiesList(
        th.Property("type", th.StringType),
        th.Property("name", th.StringType),
        th.Property("parameters", 
            th.ArrayType(
                th.ObjectType(
                    th.Property("messageValue", 
                        th.ObjectType(
                            th.Property("parameter", 
                                th.ArrayType(NESTED_PARAMETER)
                            )
                        )
                    ),
                    th.Property("name", th.StringType),
                    th.Property("value", th.StringType),
                    th.Property("multiValue", 
                        th.ArrayType(
                            th.StringType
                        )
                    ),
                    th.Property("intValue", th.StringType),
                    th.Property("multiIntValue", 
                        th.ArrayType(
                            th.StringType
                        )
                    ),
                    th.Property("boolValue", th.StringType),
                    th.Property("multiMessageValue", 
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("parameter", 
                                    th.ArrayType(NESTED_PARAMETER)
                                )
                            )   
                        )
                    ),
                ),
            )
        ),
    ).to_dict()

class UsersStream(googleWorkspaceStream):
    """Define custom stream."""

    name = "users"
    path = "directory/v1/users"
    primary_keys = ["id"]
    records_jsonpath = "$.users[*]"
    replication_key = None

    schema = th.PropertiesList(
        th.Property("kind", th.StringType),
        th.Property("id", th.StringType),
        th.Property("etag", th.StringType),
        th.Property("primaryEmail", th.StringType),
        th.Property(
            "name",
            th.ObjectType(
                th.Property("givenName", th.StringType),
                th.Property("familyName", th.StringType),
                th.Property("fullName", th.StringType),
            ),
        ),
        th.Property("isAdmin", th.BooleanType),
        th.Property("isDelegatedAdmin", th.BooleanType),
        th.Property("lastLoginTime", th.DateTimeType),
        th.Property("creationTime", th.DateTimeType),
        th.Property("agreedToTerms", th.BooleanType),
        th.Property("suspended", th.BooleanType),
        th.Property("archived", th.BooleanType),
        th.Property("changePasswordAtNextLogin", th.BooleanType),
        th.Property("ipWhitelisted", th.BooleanType),
        th.Property(
            "emails",
            th.ArrayType(
                th.ObjectType(
                    th.Property("address", th.StringType),
                    th.Property("primary", th.BooleanType),
                )
            ),
        ),
        th.Property(
            "languages",
            th.ArrayType(
                th.ObjectType(
                    th.Property("languageCode", th.StringType),
                    th.Property("preference", th.StringType),
                )
            ),
        ),
        th.Property(
            "gender",
            th.ObjectType(th.Property("type", th.StringType)),
        ),
        th.Property(
            "externalIds",
            th.ArrayType(
                th.ObjectType(
                    th.Property("value", th.StringType),
                    th.Property("type", th.StringType),
                )
            ),
        ),
        th.Property(
            "organizations",
            th.ArrayType(
                th.ObjectType(
                    th.Property("title", th.StringType),
                    th.Property("primary", th.BooleanType),
                    th.Property("customType", th.StringType),
                    th.Property("department", th.StringType),
                    th.Property("description", th.StringType),
                    th.Property("costCenter", th.StringType),
                )
            ),
        ),
        th.Property("customerId", th.StringType),
        th.Property("orgUnitPath", th.StringType),
        th.Property("isMailboxSetup", th.BooleanType),
        th.Property("isEnrolledIn2Sv", th.BooleanType),
        th.Property("isEnforcedIn2Sv", th.BooleanType),
        th.Property("includeInGlobalAddressList", th.BooleanType),
        th.Property("thumbnailPhotoUrl", th.StringType),
        th.Property("thumbnailPhotoEtag", th.StringType),
        th.Property("recoveryEmail", th.StringType),
        th.Property("recoveryPhone", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "user_id": record["id"],
        }


class UserTokensStream(googleWorkspaceStream):
    name = "user_tokens"
    parent_stream_type = UsersStream
    path = "directory/v1/users/{user_id}/tokens"
    records_jsonpath = "$.items[*]"
    replication_key = None

    schema = th.PropertiesList(
        th.Property("clientId", th.StringType),
        th.Property("scopes", th.ArrayType(th.StringType)),
        th.Property("userKey", th.StringType),
        th.Property("anonymous", th.BooleanType),
        th.Property("displayText", th.StringType),
        th.Property("nativeApp", th.BooleanType),
        th.Property("kind", th.StringType),
        th.Property("etag", th.StringType),
    ).to_dict()


class GroupsStream(googleWorkspaceStream):
    """Define custom stream."""

    name = "groups"
    path = "directory/v1/groups"
    primary_keys = ["id"]
    records_jsonpath = "$.groups[*]"
    replication_key = None

    schema = th.PropertiesList(
                    th.Property("kind", th.StringType),
                    th.Property("id", th.StringType),
                    th.Property("etag", th.StringType),
                    th.Property("email", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("directMembersCount", th.StringType),
                    th.Property("description", th.StringType),
                    th.Property("adminCreated", th.BooleanType),
    ).to_dict()

class LoginActivitiesStream(googleWorkspaceStream):

    name = "login_activities"
    path = "reports/v1/activity/users/all/applications/login"
    records_jsonpath = "$.events[*]"
    replication_key = None
    
    schema = ACTIVITIES_SCHEMA

class TokenActivitiesStream(googleWorkspaceStream):

    name = "token_activities"
    path = "reports/v1/activity/users/all/applications/token"
    records_jsonpath = "$.events[*]"
    replication_key = None
    
    schema = ACTIVITIES_SCHEMA
