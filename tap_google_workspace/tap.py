"""google-workspace tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_google_workspace.streams import (
    UsersStream, 
    GroupsStream, 
    LoginActivitiesStream, 
    TokenActivitiesStream, 
    UserTokensStream
    )

STREAM_TYPES = [
    UsersStream, 
    GroupsStream, 
    LoginActivitiesStream, 
    TokenActivitiesStream, 
    UserTokensStream
    ]


class TapgoogleWorkspace(Tap):
    """google-workspace tap class."""

    name = "tap-google-workspace"

    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("redirect_uri", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("domain", th.StringType, required=True),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapgoogleWorkspace.cli()
